"""
Functions that handle interactions with the database.
"""
import csv
import os
import logging
import itertools
from contextlib import contextmanager

import pandas as pd
import pyodbc
from dotenv import load_dotenv


load_dotenv()


class DB:
    """
    Generic class to wrap database functions.
    """

    def __init__(self):
        self._conn = self._connect()
        self.cursor = self._conn.cursor()
        self._conn.autocommit = False
        self.cursor.fast_executemany = True

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, exception_traceback):
        self.close()

    def _connect(self):
        """
        Connect to the MS SQL server
        """
        cxn_string = "DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={0},{1};DATABASE={2};UID={3};PWD={4}".format(
            os.getenv("DB_HOST"),
            os.getenv("DB_PORT"),
            os.getenv("DB_DATABASE"),
            os.getenv("DB_USER"),
            os.getenv("DB_PASSWORD"),
        )
        try:
            logging.info("Connecting to database...")
            return pyodbc.connect(cxn_string)

        except:
            logging.exception(
                f"Could not connect to database using the following connection string:\n {cxn_string}"
            )

    @property
    def connection(self):
        return self._conn

    def close(self):
        """
        Close the connection
        """
        if self.connection:
            self.connection.commit()
            self.connection.close()

    def execute(self, query):
        """
        Generic execution wrapper
        """
        try:
            with self._cursor() as cursor:
                cursor.execute(query)
        except:
            logging.exception("Exception occurred while attempting query.")

    def insert_many(self, db_table, field_names, data):
        """
        Inserts records into a database table.

        Parameters
        ----------
        db_table : str
            The name of the table to insert data into.
        field_names : tuple
            A tuple of field names, in order, to use in the
            INSERT statement. Corresponds to data parameter.
        data : list
            A list of tuples to insert into the database.

        """

        sql_statement = "insert into dbo.{} ({}) values ({})".format(
            db_table,
            ", ".join(field_names),
            ", ".join(list(itertools.repeat("?", len(field_names)))),
        )

        try:
            with self._cursor() as cursor:
                cursor.executemany(sql_statement, data)

        except:
            logging.exception(
                "Exception occurred while inserting into the database.\n{0}\n{1}".format(
                    sql_statement, data
                )
            )
        else:
            self._cursor.commit()

    def import_df(self, df, table):
        """
        Function to insert data from a dataframe using execute many.
        """

        columns = ",".join(list(df.columns.values))
        placeholders = ",".join(["?" for d in list(df.columns.values)])
        query = f"INSERT INTO {table} ({columns}) VALUES ({placeholders})"

        # with open(df, "r") as file_data:
        #     reader = csv.DictReader(df)

        # df = [tuple(x) for x in df.values]

        # self.cursor.executemany(query, df)

        for index, row in df.iterrows():
            self.cursor.execute(query, tuple(row))
            self.cursor.commit()
        # # print(query)
        # # print(df.values.tolist())

        # # try:
        #         # cursor.fast_executemany = True
        #     # print(df.head())

        # self.cursor.executemany(query, df.values.tolist())

        # # except pyodbc.IntegrityError as err:
        # #     logging.exception('Integrity error')

        # # except Exception as e:
        # #     logging.error(e)
        # #     logging.error(query)
        # #     exit()

        # # else:
        # #     logging.info("Data loaded")
        self.cursor.commit()
        # else:
        # self.connection.commit()
