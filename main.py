import os
import logging
import time

# import newrelic.agent
from db import DB
from functions.download import download
from functions.load import upload_data


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(name)s %(levelname)s: %(message)s",
    # filename="527-loader.log",
    # filemode="w",
)
logger = logging.getLogger(__name__)

# newrelic.agent.initialize('newrelic.ini')


# @newrelic.agent.background_task(name='527-loader')
def main():

    # with DB() as db:

    #     cursor = db.connection.cursor()

    #     # Run proc to prep database tables for incoming data and wait for it to complete.
    #     logging.info("Running procedure: Download_Process_01_Initialize")
    #     cursor.execute("EXEC Download_Process_01_Initialize")
    #     while cursor.nextset():
    #         time.sleep(5)
    #     cursor.commit()

    logging.info("Downloading new data file from the IRS...")
    download()

    # Upload local data to the database
    logging.info("Importing data...")
    upload_data()

    # # Run subsequent database procedures
    # procedures = [
    #     "Download_Process_02_ImportData",
    #     "Download_Process_03_ImportData_Part2",
    #     "Download_Process_04_Create_Cmtes527New",
    # ]

    # with DB() as db:
    #     cursor = db.connection.cursor()

    #     for name in procedures:

    #         logging.info(f"Running {name}")

    #         cursor.execute(f"EXEC dbo.{name}")
    #         while cursor.nextset():
    #             time.sleep(5)

    #         cursor.commit()


if __name__ == "__main__":
    main()
