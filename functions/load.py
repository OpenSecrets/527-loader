import csv
import os
import glob
import logging

import pandas as pd

from db import DB
from settings import DATA_DIR


file_to_table_map = {
    "1": "8871F",
    "D": "8871D",
    "R": "8871R",
    "E": "8871EAIN",
    "2": "8872F",
    "A": "8872A",
    "B": "8872B",
    "F": "Footer",
    "H": "Header",
}


def split_file(file_path, filename):
    """
    Takes a large text file, splits it into smaller parts, and returns a list of the new filenames.
    """

    split_files = []

    # Split large files into smaller, 800k-line files
    chunk_size = 800000

    def write_chunk(part, lines):
        partial_filename = str(part) + filename
        partial_filepath = os.path.join(DATA_DIR, partial_filename)
        split_files.append(partial_filepath)
        with open(partial_filepath, "w") as f_out:
            f_out.writelines(lines)

    with open(file_path, "rb") as f:
        count = 0
        lines = []
        for line in f:
            count += 1
            lines.append(line.decode("utf-8"))
            if count % chunk_size == 0:
                write_chunk(count // chunk_size, lines)
                lines = []
        # write remainder
        if len(lines) > 0:
            write_chunk((count // chunk_size) + 1, lines)

    os.remove(file_path)

    return split_files


def upload_data():

    # fieldnames = (
    #     "f1","f2","f3","f4","f5","f6","f7","f8","f9","f10","f11","f12",
    #     "f13","f14","f15","f16","f17","f18","f19","f20","f21","f22","f23","f24",
    #     "f25","f26","f27","f28","f29","f30","f31","f32","f33","f34","f35","f36",
    #     "f37","f38","f39","f40","f41","f42","f43","f44","f45","f46","f47","f48",
    #     "f49","f50","f521","f52","f53","f54","f55","f56","f57","f58","f59","f60"
    # )

    fieldnames = None
    field_sizes = {}
    with DB() as db:
        cursor = db.connection.cursor()
        fieldnames = [c.column_name for c in cursor.columns(table="ImportData")]
        for c in cursor.columns(table="ImportData"):
            field_sizes[c.column_name] = c.column_size
    total_lines = 0

    for filepath in glob.glob("data/raw/line_*.txt"):
        filename = filepath.split("/")[-1]
        file_list = None

        # Check if this is a large file (>200 MB) and split it into smaller ones if so.
        if os.path.getsize(filepath) > 804715200:
            logging.info(f"Splitting {filepath} into smaller files")
            file_list = split_file(filepath, filename)
        else:
            file_list = [filepath]

        for f in file_list:

            logging.info(f"Loading {f}")

            df = pd.read_csv(
                f,
                delimiter="|",
                dtype=str,
                names=fieldnames,
                header=None,
                # quotechar='"',
                encoding="utf8",
                quoting=csv.QUOTE_NONE,
            )
            df = df.fillna("")

            for fn, f_length in field_sizes.items():
                df[fn] = df[fn].str[:f_length]

            with DB() as db:
                db.import_df(df, "ImportData")

            # logging.info(f"Total lines added: {df.shape[0]}")
            total_lines += df.shape[0]

            os.remove(f)

    logging.info(
        f"Total lines loaded: {total_lines}",
    )

    upload_errors()


def upload_errors():

    logging.info("Loading line errors")

    with DB() as db:
        cursor = db.connection.cursor()
        fieldnames = [c.column_name for c in cursor.columns(table="ImportDataErrors")]

        df = pd.read_csv(
            os.path.join(DATA_DIR, "line_errors.txt"),
            delimiter="|",
            dtype=str,
            names=fieldnames,
            header=None,
            quotechar='"',
            encoding="utf8",
            quoting=csv.QUOTE_NONE,
        )
        df = df.fillna("")

        db.import_df(df, "ImportDataErrors")
