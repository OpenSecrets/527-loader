import csv
from glob import glob
import logging

import pandas as pd

from settings import IMPORT_TABLE_FIELDS


def longest_string_in_each_column(df):
    # Create a dictionary to store the results
    longest_strings = {}

    # Iterate through each column in the DataFrame
    for column in df.columns:
        # Use the apply function to find the maximum length in each column
        max_length = df[column].apply(lambda x: len(str(x))).max()

        # Find the value with the maximum length in the column
        longest_value = df[df[column].apply(lambda x: len(str(x))) == max_length][
            column
        ].iloc[0]

        # Store the result in the dictionary
        longest_strings[column] = {
            "Longest String": longest_value,
            "Length": max_length,
        }
    #         longest_strings[column] = max_length

    field_lengths = {
        "f1": 1,
        "f2": 38,
        "f3": 38,
        "f4": 70,
        "f5": 9,
        "f6": 50,
        "f7": 50,
        "f8": 50,
        "f9": 50,
        "f10": 2,
        "f11": 5,
        "f12": 4,
        "f13": 70,
        "f14": 17,
        "f15": 70,
        "f16": 8,
        "f17": 512,
    }

    # valid_values = True
    for field, max_val_info in longest_strings.items():
        if max_val_info["Length"] > field_lengths[field]:
            logging.warning(f"Value too long: {field}\n{max_val_info}")
            # valid_values = False

    #     return pd.DataFrame.from_dict(longest_strings, orient='index')
    return longest_strings


def validate_line(line, line_type):
    # field_lengths = {
    #     "f1": 1,
    #     "f2": 38,
    #     "f3": 38,
    #     "f4": 70,
    #     "f5": 9,
    #     "f6": 50,
    #     "f7": 50,
    #     "f8": 50,
    #     "f9": 50,
    #     "f10": 2,
    #     "f11": 5,
    #     "f12": 4,
    #     "f13": 70,
    #     "f14": 17,
    #     "f15": 70,
    #     "f16": 8,
    #     "f17": 512
    # }
    field_lengths = [1, 38, 38, 70, 9, 50, 50, 50, 50, 2, 5, 4, 70, 17, 70, 8, 512, 0]
    line = line.split("|")

    is_valid = True
    for index, v in enumerate(line):
        if len(v) > field_lengths[index]:
            is_valid = False

    return is_valid
