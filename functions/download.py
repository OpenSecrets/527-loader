import os
import shutil
import zipfile
import logging
import time

import requests

# import newrelic.agent

from settings import LINE_TYPES, DATA_DIR
from functions.validator import validate_line

# newrelic.agent.initialize('newrelic.ini')

# Set local data directories
os.makedirs(os.path.dirname(DATA_DIR), exist_ok=True)
RAW_DATA_DIR = os.path.join(DATA_DIR, "raw")
os.makedirs(RAW_DATA_DIR, exist_ok=True)


OUTPUT_FILES = {"error_lines": open(os.path.join(DATA_DIR, "line_errors.txt"), "w")}


# @newrelic.agent.background_task(name='527-loader')
def download():

    output_filename = os.path.join(RAW_DATA_DIR, "FullDataFile.txt")
    fetch_file(output_filename)

    with open(output_filename, "r", encoding="utf-8", newline="\n") as data_file:

        line_count = 0
        line_output = ""
        for line in data_file:
            line = line.replace("\r", " ")
            line = line.replace("\n", "")

            # Check if end-of-file
            if line == "(return status = 0)":
                continue

            row_vals = (line_output + line).split("|")
            line_type = row_vals[0]

            if line_type not in LINE_TYPES:
                continue

            row_val_count = len(row_vals)
            expected_val_count = LINE_TYPES[line_type]

            if row_val_count < expected_val_count:
                # 8871 form rows are sometimes missing meta row vals.
                if line_type == "1" and row_val_count == 41:
                    write_line(line, line_type, RAW_DATA_DIR)
                else:
                    line_output += line
                    continue
            elif row_val_count == expected_val_count:
                write_line(line_output + line, line_type, RAW_DATA_DIR)
            else:
                difference = (
                    expected_val_count - row_val_count
                )  # calculate the number of missing pipes
                pipes = ["|"] * difference
                filler = "".join(pipes)
                line_output = line_output + filler  # add missing pipes
                write_line(line_output, line_output.split("|")[0], RAW_DATA_DIR)
                write_line(line, line.split("|")[0], RAW_DATA_DIR)

            line_count += 1
            line_output = ""

        logging.info(f"File contains {line_count} lines.")
    os.remove(output_filename)


def write_line(line, line_type, output_dir):

    if line[-1:] == "|":
        line = line[:-1]

    if not line.endswith("|"):
        line = line + "|"

    if line_type == "B":
        is_valid = validate_line(line, line_type)
    else:
        is_valid = True

    if is_valid:
        if line_type not in OUTPUT_FILES:
            OUTPUT_FILES[line_type] = open(
                os.path.join(output_dir, f"line_{line_type}.txt"), "w"
            )

        OUTPUT_FILES[line_type].write(line + "\n")
    else:
        OUTPUT_FILES["error_lines"].write(line + "\n")
        logging.info(f"Found invalid line\n{line}")


def fetch_file(output_filename):

    # Fetch zipfile
    zip_filename = "527data.zip"

    try:
        with requests.get(
            "https://forms.irs.gov/app/pod/dataDownload/fullData"
        ) as file_r:
            with open(zip_filename, "wb") as zfile:
                zfile.write(file_r.content)

            file_downloading = True
            while file_downloading:
                time.sleep(5)
                print("Waiting for file to download...")
                if os.path.exists(zip_filename):
                    break
    except:
        logging.exception("Error fetching zipfile from IRS.")

    # Unzip and extract file
    with zipfile.ZipFile(zip_filename) as zip_file:

        source = zip_file.open("var/IRS/data/scripts/pofd/download/FullDataFile.txt")
        target = open(output_filename, "wb")
        logging.info("Unzipping data.")
        with source, target:
            shutil.copyfileobj(source, target)
            os.remove(zip_filename)
