# IRS 527 organization loader

A script to fetch, parse and load IRS data for 527 organizations.


## Installation
Install with pip/virtual environment
`pip install -r requirements.txt`

Install with poetry
`poetry install`

## Usage
The `main.py` script fetches the latest data file from the IRS and parses it into separate files based on the record type.

TODO: Add database load.

## About the source data
The IRS makes data available in a [single file](https://forms.irs.gov/app/pod/dataDownload/dataDownload) that comprises electronically submitted Forms 8871, Forms 8872, and associated Schedule A and Schedule B items. The file as essentially a flattened version of the IRS's 527 database and each new line is a different record type as defined [here](main.py#14). Information about the data layout can be found [here](https://forms.irs.gov/app/pod/dataDownload/dataLayout).


## Current process
1. Download file
2. Check it for truncated lines
3. Check downloaded data's line count
4. Create `ImportData_$DATE` table with date the data was downloaded
5. Run [Download_Process_01_Initialize](db_procedures.md#Download_Process_01_Initialize) procedure to create new ImportData table and empty working tables
6. Import all data into `ImportData_$DATE`, keeping track of how many lines have been processed and if they match the line count in the original file.
    - We can maybe bypass this and the following #7 based on how we're parsing the data now.
7. Run [Download_Process_02_ImportData](db_procedures.md#Download_Process_02_ImportData) procedure to split data from #6 into the `Working_XXXX` tables. It also creates 3 tables for 8871D, 8871F and 8872F new records only.
8. Run [Download_Process_03_ImportData_Part2](db_procedures.md#Download_Process_03_ImportData_Part2) procedure. For 8872A and 8872B, this updates existing records, adds new records and marks existing records no longer present in the data. For 8872F, it saves past information about the AmendAction_SchA(B) fields before rebuilding.  For all other tables, it deletes and then appends the records, except for the Header file, which is simply added so that you have a record of processing dates.
9. Run [Download_Process_04_Create_Cmtes527New](db_procedures.md#Download_Process_04_Create_Cmtes527New). There is a list of new cmtes that require coding within the stored procedure output. Check to be sure the current Cycle is included on this and subsequent steps.
10. Delete `Cmtes527_old`, Rename `Cmtes527` to `Cmtes527_old` and then Rename `Cmtes527New` to `Cmtes527`
11. Run `Download_Process_05_ImportData_Amendments`
